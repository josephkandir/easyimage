﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasyImage.Interfaces
{
    public interface IEasyImage
    {
        string Upload(IFormFile file, bool autoFileName, string configPath = "imageDirectory", string? pathPrefix = "");
    }
}
