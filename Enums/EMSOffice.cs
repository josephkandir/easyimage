﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyImage.Enums
{
    public enum EMSOffice
    {
        WORD, EXCEL, POWERPOINT, ACCESS
    }
}
